﻿using System;
using System.Collections.Generic;

namespace BSQ
{
	class MainClass
	{
		private static List<string> generateMap(int width, int height, int density)
		{
			Random random = new Random();
			List<string> map;

			map = new List<string>();
			for (int y = 0; y < height; y++)
			{
				string line = "";
				for (int x = 0; x < width; x++)
				{
					if (random.Next() % density != 3 && random.Next() % density != 14)
					{
						line += ".";
					}
					else
					{
						line += "O";
					}
				}
				map.Add(line);
			}
			return map;
		}

		public static void Main(string[] args)
		{
			List<string> map = generateMap(100, 100, 20);

			foreach (string line in map)
			{
				Console.Write(line);
				Console.WriteLine();
			}
		}
	}
}
